#include <gtk/gtk.h>

static int count = 0;
static gboolean answ;
static gchar times[10] = {0};

GtkWidget *g_lbl_num;
GtkWidget *g_lbl_amount;

int main(int argc, char *argv[]) {

    GtkBuilder      *builder; 
    GtkWidget       *window;

    gtk_init(&argc, &argv);

    builder = gtk_builder_new();
    gtk_builder_add_from_file (builder, "glade/window_main.glade", NULL);

    window = GTK_WIDGET(gtk_builder_get_object(builder, "window_main"));
    gtk_builder_connect_signals(builder, NULL);
    
    // sets up vars with widget data
    g_lbl_num = GTK_WIDGET(gtk_builder_get_object(builder, "lbl_num"));
    g_lbl_amount = GTK_WIDGET(gtk_builder_get_object(builder, "lbl_amount"));

    g_object_unref(builder);

    gtk_widget_show(window);                
    gtk_main();

    return 0;
}

void on_btn_flip_clicked(void) {
    answ = g_random_boolean();
    count++;
    sprintf(times, "%d", count);

    gtk_label_set_text(GTK_LABEL(g_lbl_amount), times);
    printf("[*] Press No. %s\n", times);

    switch (answ) {
        case TRUE :
            gtk_label_set_text(GTK_LABEL(g_lbl_num), "Heads");
            break;
        case FALSE :
            gtk_label_set_text(GTK_LABEL(g_lbl_num), "Tails");
            break;
    }
}

void on_window_main_destroy() {

    gtk_main_quit();
}
